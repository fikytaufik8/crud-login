<?php

namespace App\Models;

use CodeIgniter\Model;

class EmployeeModel extends Model
{
    protected $table = 'employee';
    protected $useTimestamps = true;
    protected $allowedFields = ['name', 'email', 'position', 'salary','photo'];
}