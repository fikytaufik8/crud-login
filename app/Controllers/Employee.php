<?php

namespace App\Controllers;
use App\Models\EmployeeModel;

class Employee extends BaseController
{
    protected $employeeModel;

    public function __construct(){
        $this->employeeModel =  new EmployeeModel();
    }
    public function index()
    {

        $employee = $this->employeeModel->findAll();

        $data = [
            'title' => 'Employee',
            'employee' => $employee
        ];

        return view('employee/index', $data);
    }

    public function create(){
        $data = [
            'title' => 'Add Employee',
            'validation' => \Config\Services::validation()
        ];

        return view('employee/create',$data);
    }

    public function store(){
        if(!$this->validate([
            'name' => 'required',
            'email' => [
                'rules' => 'required|is_unique[employee.email]',
                'errors' => [
                    'required' => 'The {field} field is required.',
                    'is_unique' => 'The {field} field is unique.'
                ],
            ],
            'position' => 'required',
            'salary' => 'required',
            // 'photo' => [
            //     'rules' => 'uploaded[photo]',
            // ]
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('employee/create')->withInput()->with('validation', $validation);
        }

        $photo = $this->request->getFile('photo');

        if($photo->getError() == 4){
            $photoName = 'user.png';
            $photoUrl = 'img/'.$photoName;
        }else{
            $photoName = $photo->getRandomName();
            $photo->move('img', $photoName);
            $photoUrl = 'img/'.$photoName;
        }

        $this->employeeModel->save([
            'name' => $this->request->getVar('name'),
            'email' => $this->request->getVar('email'),
            'position' => $this->request->getVar('position'),
            'salary' => $this->request->getVar('salary'),
            'photo' => $photoUrl
        ]);

        session()->setFlashdata('message', 'Data Saved Successfully.');

        return redirect()->to('employee');
    }

    public function edit($id){
        $data = [
            'title' => 'Edit Employee',
            'validation' => \Config\Services::validation(),
            'employee' => $this->employeeModel->find($id)
        ];

        return view('employee/edit',$data);
    }

    public function update($id){
        $email = $this->employeeModel->find($id);
        if($email['email'] == $this->request->getVar('email')){
            $ruleEmail = 'required';
        }else{
            $ruleEmail = 'required|is_unique[employee.email]';
        }

        if(!$this->validate([
            'name' => 'required',
            'email' => [
                'rules' => $ruleEmail,
                'errors' => [
                    'required' => 'The {field} field is required.',
                    'is_unique' => 'The {field} field is unique.'
                ],
            ],
            'position' => 'required',
            'salary' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('employee/edit/'.$id)->withInput()->with('validation', $validation);
        }

        $employee = $this->employeeModel->find($id);
        $photoUrl = $employee['photo'];
        
        if(strlen($_FILES['photo']['tmp_name']) > 0){

            if(!$employee['photo'] !== 'img/user.png'){
                unlink($employee['photo']);
            }

            $photo = $this->request->getFile('photo');

            if($photo->getError() == 4){
                $photoName = 'user.png';
                $photoUrl = 'img/'.$photoName;
            }else{
                $photoName = $photo->getRandomName();
                $photo->move('img', $photoName);
                $photoUrl = 'img/'.$photoName;
            }
        }

        $this->employeeModel->save([
            'id' => $id,
            'name' => $this->request->getVar('name'),
            'email' => $this->request->getVar('email'),
            'position' => $this->request->getVar('position'),
            'salary' => $this->request->getVar('salary'),
            'photo' => $photoUrl
        ]);

        session()->setFlashdata('message', 'Data Updated Successfully.');

        return redirect()->to('employee');
    }

    public function delete($id){
        $employee = $this->employeeModel->find($id);

        $this->employeeModel->delete($id);

        if(!$employee['photo'] !== 'img/user.png'){
            unlink($employee['photo']);
        }

        session()->setFlashdata('message', 'Data Deleted Successfully.');
        return redirect()->to('employee');
    }
}
