
<?= $this->extend('layouts/app'); ?>

<?= $this->section('content'); ?>
<div class="app-title">
    <div>
        <h1><i class="bi bi-person"></i> <?= $title; ?></h1>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="bi bi-house-door fs-6"></i></li>
        <li class="breadcrumb-item">Master Data</li>
        <li class="breadcrumb-item active"><a href="#">Employee</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <form action="/employee/update/<?= $employee['id'];?>" method="post" enctype="multipart/form-data">
            <?= csrf_field(); ?>
                <input type="hidden" name="_method" value="PUT" />
                <input type="hidden" name="id" value="<?php echo $employee['id']; ?>">
                <div class="tile-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label class="form-label" for="name">Full Name <?= $validation->hasError('name')?></label>
                                <?= $validation->hasError('name'); ?>
                                <input class="form-control <?= (session('validation')) ? (session('validation')->getError('name')) ? 'is-invalid' : '' : '';?>" value="<?= (old('name')) ? old('name') : $employee['name'] ?>" id="name" type="text" placeholder="Full Name" name="name" autofocus>
                                <small class="form-text text-danger">
                                    <?php if (session('validation')) : ?>
                                        <?= session('validation')->getError('name') ?>
                                    <?php endif ?>
                                </small>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="email">Email address</label>
                                <input class="form-control <?= (session('validation')) ? (session('validation')->getError('email')) ? 'is-invalid' : '' : '';?>" value="<?= (old('email')) ? old('email') : $employee['email'] ?>" id="email" type="email" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                                <small class="form-text text-danger" id="emailHelp">
                                    <?php if (session('validation')) : ?>
                                        <?= session('validation')->getError('email') ?>
                                    <?php endif ?>
                                </small>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="position">Position</label>
                                <input class="form-control <?= (session('validation')) ? (session('validation')->getError('position')) ? 'is-invalid' : '' : '';?>" value="<?= (old('position')) ? old('position') : $employee['position'] ?>" id="position" type="text" placeholder="Position" name="position">
                                <small class="form-text text-danger" id="emailHelp">
                                <?php if (session('validation')) : ?>
                                    <?= session('validation')->getError('position') ?>
                                <?php endif ?>
                                </small>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="salary">Salary</label>
                                <input class="form-control <?= (session('validation')) ? (session('validation')->getError('salary')) ? 'is-invalid' : '' : '';?>" value="<?= (old('salary')) ? old('salary') : $employee['salary'] ?>" id="salary" type="number" placeholder="Salary" name="salary">
                                <small class="form-text text-danger" id="emailHelp">
                                    <?php if (session('validation')) : ?>
                                        <?= session('validation')->getError('salary') ?>
                                    <?php endif ?>
                                </small>
                            </div>
                            <div class="mb-3">
                                <label class="form-label" for="photo">Photo</label>
                                <input id="photo" type="file" name="photo" class="form-control dropify" data-max-file-size="300KB" data-allowed-file-extensions="jpg jpeg png" accept=".png, .jpg, .jpeg" data-default-file="<?= base_url($employee['photo']) ?>">
                                <small class="text-mutedr">*  Max file size <b>300KB</b> | Image Only | Recomended Size <b>100px x 100px</b></small>
                                <strong class="form-text text-danger" id="emailHelp">
                                    <?php if (session('validation')) : ?>
                                        <?= session('validation')->getError('photo') ?>
                                    <?php endif ?>
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tile-footer">
                <button class="btn btn-primary" type="submit">Submit</button>
                <a href="/employee" class="btn btn-secondary" type="submit">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>