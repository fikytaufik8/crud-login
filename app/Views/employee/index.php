
<?= $this->extend('layouts/app'); ?>

<?= $this->section('content'); ?>
<div class="app-title">
    <div>
        <h1><i class="bi bi-person"></i> <?= $title; ?></h1>
    </div>
    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="bi bi-house-door fs-6"></i></li>
        <li class="breadcrumb-item">Master Data</li>
        <li class="breadcrumb-item active"><a href="#">Employee</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <?php if(session()->getFlashdata('message')) : ?>
                    <div class="bs-component">
                        <div class="alert alert-dismissible alert-success">
                            <button class="btn-close" type="button" data-bs-dismiss="alert"></button>
                            <?= session()->getFlashdata('message') ?>
                        </div>
                    </div>
                <?php endif;?>
                <a href="employee/create" class="btn btn-primary mb-3"><i class="icon bi bi-plus"></i> Add Employee</a>
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="10%">Photo</th>
                                <th>Full Name</th>
                                <th>Position</th>
                                <th>Email</th>
                                <th>Salary</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  $i = 1; ?>
                            <?php foreach($employee as $value ) : ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><img src="<?php echo base_url($value['photo']); ?>" alt="Photo" width="50px" height="50px"></td>
                                <td><?= $value['name']; ?></td>
                                <td><?= $value['position']; ?></td>
                                <td><?= $value['email']; ?></td>
                                <td><?= $value['salary']; ?></td>
                                <td>
                                    <form action="/employee/delete/<?= $value['id'];?>" id="delete-form-<?= $value['id'] ?>" method="post" style="display: inline-block">
                                        <?= csrf_field(); ?>
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="btn btn-primary" type="button" onclick="confirmBox(<?= $value['id']?>)">Delete</button>
                                    </form>
                                    <a href="employee/edit/<?= $value['id']?>" class="btn btn-primary">Edit</a>
                                </td>
                            </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function confirmBox(id){
            var link = $(this);

            Swal.fire({
                title: "Confirmation Delete",
                text: "Are you sure to delete this Data ?",
                type: "warning",
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
                }).then((result) => {
                    if (result.value) {
                        $("#delete-form-"+id).submit();
                    }else{
                        Swal.fire("Cancelled","Data deletion Cancelled", "info");
                    }
                });
        }
</script>
<?= $this->endSection(); ?>